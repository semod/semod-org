.PHONY: all puml clean-puml site docker

all: docker

###
### Компиляция диаграмм PlantUML в SVG
###

puml_src_dir := .
puml_svg_dir := .

puml_src := $(shell find $(puml_src_dir) -type f -name '*.puml' -not -path './_lib/*' -not -path './_theme.puml')
puml_svg := $(patsubst $(puml_src_dir)/%.puml, $(puml_svg_dir)/%.svg, $(puml_src))

# Compile (generation of .svg)
$(puml_svg): $(puml_svg_dir)/%.svg: $(puml_src_dir)/%.puml
	plantuml -tsvg -o $(abspath $(@D)) $<
	svgo --multipass $@

puml: $(puml_svg)

clean-puml:
	rm -rf $(puml_svg)

site:
	emacs --batch --no-init-file --load publish.el --funcall org-publish-all

docker: puml
	docker run -it --rm \
		--workdir /home \
		--volume $(PWD):/home \
		silex/emacs:master-alpine-ci \
		make site

